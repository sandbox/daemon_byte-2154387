<?php
/**
 * @file
 * Builds placeholder replacement tokens
 */

/**
 * Implements hook_token_info().
 */
function i18n_tokens_token_info() {
  // node related variables.
  $node['i18n-name'] = array(
    'name' => t("Content type name (localized)"),
    'description' => t("The name of the content type."),
  );

  return array(
    'tokens' => array(
      'content-type' => $node,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function i18n_tokens_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);
  $langcode = isset($options['language']) ? $options['language']->language : i18n_langcode();
  if ($type == 'content-type' && !empty($data['node_type'])) {
    $node_type = $data['node_type'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'i18n-name':
          $tname = i18n_string_text(array('node', 'type', $node_type->type, 'name'), $node_type->name, array('langcode' => $langcode, 'sanitize' => $sanitize, 'cache' => TRUE));
          $replacements[$original] = $sanitize ? check_plain($tname) : $tname;
          break;
      }
    }
  }

  return $replacements;
}